<?php
// source: layout.latte

use Latte\Runtime as LR;

class Templatececa929251 extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="/public/css/style.css">
	<link href='https://fonts.googleapis.com/css?family=Nunito' rel='stylesheet'>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<title><?php
		$this->renderBlock('title', $this->params, 'html');
?></title>
    <?php
		if (isset($this->blockQueue["meta"])) {
			$this->renderBlock('meta', $this->params, 'html');
		}
?>

</head>
<body>
	<main>
		<header id="auth">
			<nav>
				<ul>
					<a href="<?php
		echo $router->pathFor("index");
?>"><li><span class="material-icons">home</span></li></a>
<?php
		if (isset($prihlaseny_uzivatel['id_uzivatel']) && $prihlaseny_uzivatel['id_uzivatel'] != null) {
			?>            			<a href="<?php
			echo $router->pathFor("odhlasit");
			?>" title="Kliknutím se odhlásíte."><li><span><?php echo LR\Filters::escapeHtmlText($prihlaseny_uzivatel['id_uzivatel']) /* line 19 */ ?></span></li></a>
            				<!--<a title="Přidání kategorií a produktů."><li><span class="material-icons">widgets</span></li></a>-->
	            			<a href="<?php
			echo $router->pathFor("pridat_kategorie");
?>" title="Přidání kategorií."><li>Přidat kategorii</li></a>
	            			<a href="<?php
			echo $router->pathFor("pridat_produkt");
?>" title="Přidání produktů."><li>Přidat produkt</li></a>
<?php
		}
		else {
			?>            			<a href="<?php
			echo $router->pathFor("prihlasit");
?>"><li><span>Přihlásit se</span></li></a>
<?php
		}
?>
				</ul>
			</nav>
		</header>
		<section id="logo">
			<p><h1 class="noselect">Komparátor cen</h1></p>
			<hr>
			<p><h2 class="noselect">Projekt do VWA</h2></p>
		</section>
<?php
		if (isset($prihlaseny_uzivatel['id_uzivatel']) && $prihlaseny_uzivatel['id_uzivatel'] != null) {
			?>		<a href="<?php
			echo $router->pathFor("oblibene");
?>"><span id="favorite" class="material-icons">favorite</span></a>
<?php
		}
		$this->renderBlock('body', $this->params, 'html');
?>
	</main>
</body>
</html><?php
		return get_defined_vars();
	}

}
