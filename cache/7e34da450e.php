<?php
// source: index.latte

use Latte\Runtime as LR;

class Template7e34da450e extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Index<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <section id="navigace">
         <nav>
            <ul>
               <a href="<?php
		echo $router->pathFor("kategorie");
?>"><li>Kategorie</li></a>
            </ul>
         </nav>
      </section>
    <section class="zadani">
       <p>Potřebujeme vytvořit webovou aplikaci, která bude spravovat online nabídku produktů. Uživatelé bez přihlášení budou moci prohlížet v jednotlivých kategoriích produktů. U každého produktu budeme ukládat, jeho název, výrobce, bude nás ale také zajímat vývoj cen na trhu. Budeme tedy potřebovat ukládat aktuální ceny k určitému datu. Návštěvník našeho webu pak bude mít možnost historii vývoje cen produktů zobrazit.</p><p>Produkty vždy budou spadat do určité kategorie, která bude charakterizovaná svým názvem a popisem. Vytvářet kategorie, vkládat nové produkty může pouze přihlášený uživatel (Administrátor). Ceny produktů budou moci vkládat i nepřihlášení uživatelé. Pokud takový uživatel bude chtít vložit cenu, bude muset do komentáře napsat, kde je tato cena uvedena (například Obchod, či E-shop). Vymazat vloženou cenu bude mít možnost zase pouze administrátor.</p>
   </section>
   <section class="obrazky">
      <h2>ER diagram</h2>
      <img src="/public/img/erd.png">
   </section>
   <section class="obrazky">
      <h2>Use Case diagram</h2>
      <img src="/public/img/uml.png">
   </section>
<?php
	}

}
