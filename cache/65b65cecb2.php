<?php
// source: produkty.latte

use Latte\Runtime as LR;

class Template65b65cecb2 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['p'])) trigger_error('Variable $p overwritten in foreach on line 12');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Produkty<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
   <main>
      <h1>Produkty</h1>
      <!--<?php
		if ($message) {
?>

         <p><?php echo LR\Filters::escapeHtmlComment($message) /* line 9 */ ?></p>
      <?php
		}
?>-->
         <section>
<?php
		$iterations = 0;
		foreach ($produkt as $p) {
?>
               <form method="post">
                  <input type="hidden" name="id_produkt" value="'<?php echo LR\Filters::escapeHtmlAttr($p['id_produkt']) /* line 14 */ ?>'">
                  <h2 class="kategorie"></a><?php echo LR\Filters::escapeHtmlText($p['nazev']) /* line 15 */ ?></h2>
               </form>
               <p>Výrobce: <?php echo LR\Filters::escapeHtmlText($p['vyrobce']) /* line 17 */ ?></p>
               <a class="prejit" href="<?php
			echo $router->pathFor("cena", ['nazev_produkt'=>$p['nazev']], ['id_produkt'=>$p['id_produkt']]);
			?>">Přejít na cenu produktu <?php echo LR\Filters::escapeHtmlText($p['nazev']) /* line 18 */ ?></a>
<?php
			$iterations++;
		}
?>
      </section>
   </main>
<?php
	}

}
