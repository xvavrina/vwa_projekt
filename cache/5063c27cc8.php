<?php
// source: pridat_kategorie.latte

use Latte\Runtime as LR;

class Template5063c27cc8 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Přidat produkt<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
<main>
  <h1>Přidání kategorie</h1>
  <!--<?php
		if ($message) {
			?><p><?php echo LR\Filters::escapeHtmlComment($message) /* line 6 */ ?></p><?php
		}
?>-->
  <section>
    <form method="post">
    <table>
      <tr>
        <td>Název:</td>
        <td><input type="text" name="nazev" required></td>
      </tr>
      <tr>
        <td>Popis:</td>
        <td><textarea id="wide_text" type="text" name="popis" required></textarea></td>
      </tr>
    </table>
    <button type="submit" class="form_send">Odeslat</button>
    </form>
  </section>
</main> 
</main> 
<?php
	}

}
