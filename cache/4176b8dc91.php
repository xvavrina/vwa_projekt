<?php
// source: oblibene.latte

use Latte\Runtime as LR;

class Template4176b8dc91 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['p'])) trigger_error('Variable $p overwritten in foreach on line 18');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Oblíbené<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
   <main>
      <h1>Oblíbené</h1>
<?php
		if (isset($message)) {
			?>         <p><?php echo LR\Filters::escapeHtmlText($message) /* line 9 */ ?></p>
<?php
		}
?>
      <section>
         <table class="cena">
            <tr>
               <th>Název</th>
               <th>Výrobce</th>
               <th>Odebrat</th>
               </tr> 
<?php
		$iterations = 0;
		foreach ($produkt as $p) {
?>
               <tr>
                  <td><?php echo LR\Filters::escapeHtmlText($p['nazev']) /* line 20 */ ?></td>
                  <td><?php echo LR\Filters::escapeHtmlText($p['vyrobce']) /* line 21 */ ?></td>
                  <td><a href="<?php
			echo $router->pathFor("oblibene_odebrat", ['id_oblibene'=>$p['id_oblibene']]);
?>"><span class="material-icons">delete</span></a></td>
               </tr> 
<?php
			$iterations++;
		}
?>
    </table>
  </section>
   </main>
<?php
	}

}
