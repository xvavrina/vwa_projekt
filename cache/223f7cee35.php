<?php
// source: prihlasit.latte

use Latte\Runtime as LR;

class Template223f7cee35 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Přihlášení<?php
	}


	function blockBody($_args)
	{
?>   <main>
      <h1>Přihlášení</h1>
      <section>
         <form method="post">
            <table class="prihlaseni">
               <tr>
                  <td>Přihlašovací jméno:</td>
                  <td><input type="text" name="id_prihlasovaci" required></td>
               </tr>
               <tr>
                  <td>Heslo:</td>
                  <td><input type="password" name="heslo" required></td>
               </tr>
               <tr>
                  <td class="colspan" colspan="2"><input type="submit" value="Přihlásit"></td>
               </tr>
            </table>
         </form>
      </section>
   </main>  
<?php
	}

}
