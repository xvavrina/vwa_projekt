<?php
// source: pridat_produkt.latte

use Latte\Runtime as LR;

class Template62313cc2e8 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['k'])) trigger_error('Variable $k overwritten in foreach on line 13');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Přidat produkt<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
<main>
  <h1>Přidání produktu</h1>
  <!--<?php
		if ($message) {
			?><p><?php echo LR\Filters::escapeHtmlComment($message) /* line 6 */ ?></p><?php
		}
?>-->
  <section>
    <form method="post">
    <table>
      <tr>
        <td>Kategorie:</td>
        <td><select name="id_kategorie">
<?php
		$iterations = 0;
		foreach ($kategorie as $k) {
			?>          <option title="<?php echo LR\Filters::escapeHtmlAttr($k['popis']) /* line 14 */ ?>" value="<?php
			echo LR\Filters::escapeHtmlAttr($k['id_kategorie']) /* line 14 */ ?>">
            <?php echo LR\Filters::escapeHtmlText($k['nazev']) /* line 15 */ ?>

          </option>
<?php
			$iterations++;
		}
?>
        </select></td>
      </tr>
      <tr>
        <td>Název:</td>
        <td><input type="text" name="nazev" required></td>
      </tr>
      <tr>
        <td>Výrobce:</td>
        <td><input type="text" name="vyrobce" required></td>
      </tr>
    </table>
    <td><button type="submit" class="form_send">Odeslat</button></td>
    </form>
  </section>
</main> 
<?php
	}

}
