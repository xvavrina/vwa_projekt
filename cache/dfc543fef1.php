<?php
// source: nespravne_prihlaseni.latte

use Latte\Runtime as LR;

class Templatedfc543fef1 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Nesprávné přihlášení<?php
	}


	function blockBody($_args)
	{
?>    <section id="nespravne">
          <h1><span class="material-icons">warning</span> Nesprávné přihlášení! :( <span class="material-icons">warning</span></h1>
          <h2>Zkuste se znovu přihlásit. :)</h2>
    </section>
<?php
	}

}
