<?php
// source: kategorie.latte

use Latte\Runtime as LR;

class Template23ceb14901 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['k'])) trigger_error('Variable $k overwritten in foreach on line 9');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Kategorie<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
   <main>
      <h1>Kategorie</h1>
         <section>
<?php
		$iterations = 0;
		foreach ($kategorie as $k) {
			?>               <h2 class="kategorie"><?php echo LR\Filters::escapeHtmlText($k['nazev']) /* line 10 */ ?></h2>
               <p><?php echo LR\Filters::escapeHtmlText($k['popis']) /* line 11 */ ?></p>
               <a class="prejit" href="<?php
			echo $router->pathFor("produkty", ['nazev_kategorie'=>$k['nazev']], ['id_kategorie'=>$k['id_kategorie']]);
			?>">Přejít na <?php echo LR\Filters::escapeHtmlText($k['nazev']) /* line 12 */ ?></a>
<?php
			$iterations++;
		}
?>
         </section>
   </main>
<?php
	}

}
