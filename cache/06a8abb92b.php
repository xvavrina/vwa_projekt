<?php
// source: sprava.latte

use Latte\Runtime as LR;

class Template06a8abb92b extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Správa<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
<main>
  <h1>Správa</h1>
  <!--<?php
		if ($message) {
			?><p><?php echo LR\Filters::escapeHtmlComment($message) /* line 6 */ ?></p><?php
		}
?>-->
  <div class="sprava">
    <section class="karta">
    <h2>Přidat kategorii?</h2>
    <hr>
    <a class="prejit"></a>
  </section>
  <section class="karta">
    <h2>Přidat produkt?</h2>
    <hr>
    <a class="prejit">Přidat produkt do </a>
  </section>
  </div>
  
</main> 
<?php
	}

}
