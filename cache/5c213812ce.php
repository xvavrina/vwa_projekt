<?php
// source: cena.latte

use Latte\Runtime as LR;

class Template5c213812ce extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['p'])) trigger_error('Variable $p overwritten in foreach on line 17');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Cena produktu<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
<main>
  <h1><?php echo LR\Filters::escapeHtmlText($nazev_produkt) /* line 5 */ ?></h1><?php
		if (isset($prihlaseny_uzivatel['id_uzivatel']) && $prihlaseny_uzivatel['id_uzivatel'] != null) {
			?><a href="<?php
			echo $router->pathFor("oblibene_pridat", ['id_produkt'=>$id_produkt]);
			?>"><span id="oblibene_produkt" class="material-icons">favorite</span></a><?php
		}
?>

  <!--<?php
		if (isset($message)) {
			?><p><?php echo LR\Filters::escapeHtmlComment($message) /* line 6 */ ?></p><?php
		}
?>-->
  <section>
    <table class="cena">
      <tr>
        <th>Datum</th>
        <th>Cena</th>
<?php
		if (isset($prihlaseny_uzivatel['id_uzivatel'])) {
?>
        	<th>Odebrat</th>
          <p><?php echo LR\Filters::escapeHtmlText($nazev_produkt) /* line 14 */ ?></p>
<?php
		}
?>
      </tr> 
<?php
		$iterations = 0;
		foreach ($produkt as $p) {
?>
      <tr>
        <td><?php echo LR\Filters::escapeHtmlText($p['datum']) /* line 19 */ ?></td>
        <td><?php echo LR\Filters::escapeHtmlText($p['cena']) /* line 20 */ ?> Kč</td>
<?php
			if (isset($prihlaseny_uzivatel['id_uzivatel'])) {
				?>        	<td><a href="<?php
				echo $router->pathFor("smazat_cena", ['id_cena'=>$p['id_cena'], 'nazev_produkt'=>$nazev_produkt]);
?>" onclick="return confirm ('Jste si jistý?')"><span class="material-icons">delete</span></a></td>
<?php
			}
?>
      </tr> 
<?php
			$iterations++;
		}
?>
    </table>
  </section>
  <section>
  	<form method="post">
  		<table>
	      <tr>
	      	<input type="hidden" name="id_produkt" value="<?php echo LR\Filters::escapeHtmlAttr($produkt[0]['id_produkt']) /* line 32 */ ?>">
	        <td>Cena:</td>
	        <td><input type="number" step="0.1" name="cena" required></td>
	      </tr>
	      <tr>
	        <td>Datum:</td>
	        <td><input type="date" name="datum" required></td>
	      </tr>
<?php
		if (!isset($prihlaseny_uzivatel['id_uzivatel'])) {
?>
	      	<tr>
        		<td>Komentář:</td>
        		<td><textarea id="wide_text" type="text" name="komentar" required></textarea></td>
      		</tr>
<?php
		}
?>
	    </table>
	    <td><button type="submit" class="form_send">Odeslat</button></td>
	    </form>
  </section>
</main> 
<?php
	}

}
