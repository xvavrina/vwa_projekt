<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

//Index - hlavní stránka
$app->get('/', function (Request $request, Response $response, $args) {
    // Render index view
    return $this->view->render($response, 'index.latte');
})->setName('index');

//Přihlášení - stránka
$app->get('/prihlasit', function (Request $request, Response $response, $args) {
    return $this->view->render($response, 'prihlasit.latte');
})->setName('prihlasit');

//POST na prihlaseni
$app->post('/prihlasit', function (Request $request, Response $response, $args) {
    $formData=$request->getParsedBody();

    $password_hash = hash('sha256',$formData['heslo']);

    $stmt = $this->db->prepare('SELECT * FROM uzivatele WHERE lower(id_uzivatel)=lower(:id) AND heslo = :h');

    $stmt->bindValue(':id',$formData['id_prihlasovaci']);
    $stmt->bindValue(':h',$password_hash);

    $stmt->execute();

    $prihlaseny_uzivatel = $stmt->fetch();

    if($prihlaseny_uzivatel){
        $_SESSION['prihlaseny_uzivatel'] = $prihlaseny_uzivatel;
        return $response->withHeader('location',$this->router->pathFor('index'));
    }
    else{
        return $this->view->render($response, 'nespravne_prihlaseni.latte');   
    }
});

//***--->PŘIHLÁŠENÝ GROUP<---***
$app->group('/prihlaseny',function() use($app){

//Odhlášení (sežrání session)
    $app->get('/odhlasit', function (Request $request, Response $response, $args) {
        session_destroy();
        return $response->withHeader('location',$this->router->pathFor('prihlasit'));
    })->setName('odhlasit');
//Správa - Přidání cen, kategorií a produktů
    $app->get('/pridat', function (Request $request, Response $response, $args) {
    return $response->withHeader('location',$this->router->pathFor('index'));
    });

    $app->get('/pridat/produkt', function (Request $request, Response $response, $args) {
        $stmt = $this->db->prepare('SELECT nazev, popis, id_kategorie FROM kategorie');
        $stmt->execute();
        $data['kategorie']=$stmt->fetchall();
    return $this->view->render($response, 'pridat_produkt.latte', $data);
    })->setName('pridat_produkt');
//Přidání produktu
    $app->post('/pridat/produkt', function (Request $request, Response $response, $args) {
        $formData=$request->getParsedBody();
        if(!empty($formData['nazev'] && $formData['vyrobce'])){
            $stmt = $this->db->prepare('INSERT INTO produkt(id_kategorie, nazev, vyrobce) VALUES(:idk,:n,:v)');
            $stmt->bindParam(':idk',$formData['id_kategorie']);
            $stmt->bindParam(':n',$formData['nazev']);
            $stmt->bindParam(':v',$formData['vyrobce']);

            $stmt->execute();
        }
        else{
            return $response->withHeader('Location',$this->router->pathFor('pridat_produkt'));
        }
    return $response->withHeader('Location',$this->router->pathFor('pridat_produkt'));
    });
//Přidání kategorie
    $app->get('/pridat/kategorie', function (Request $request, Response $response, $args) {
    return $this->view->render($response, 'pridat_kategorie.latte');
    })->setName('pridat_kategorie');

    $app->post('/pridat/kategorie', function (Request $request, Response $response, $args) {
        $formData=$request->getParsedBody();
        if(!empty($formData['nazev'] && $formData['popis'])){
            $stmt = $this->db->prepare('INSERT INTO kategorie(nazev, popis) VALUES(:n,:p)');
            $stmt->bindParam(':n',$formData['nazev']);
            $stmt->bindParam(':p',$formData['popis']);

            $stmt->execute();
        }
        else{
            return $response->withHeader('Location',$this->router->pathFor('pridat_kategorie'));
        }
    return $this->view->render($response, 'pridat_kategorie.latte');
    });
//Odebrání ceny
    $app->get('/{nazev_produkt}/{id_cena}/smazat', function (Request $request, Response $response, $args) {   
    $data['nazev_produkt'] = $args['nazev_produkt'];
    try{
        $stmt=$this->db->prepare('DELETE FROM cena_produkt WHERE id_cena = :idc');
        $stmt->bindParam(':idc',$args['id_cena']);
        $stmt->execute();
    }
    catch(Exception $e){
        echo var_dump($e);
    }
    $nzv=$args['nazev_produkt'];
    $stmt = $this->db->prepare('SELECT id_cena, nazev, vyrobce, cena, datum from produkt NATURAL JOIN cena_produkt WHERE (:n = produkt.nazev) ORDER BY datum asc');
    $stmt->bindValue(':n',$nzv);
    $stmt->execute();
    $data['produkt'] = $stmt->fetchall();

    $stmt = $this->db->prepare('SELECT id_produkt FROM produkt WHERE nazev=:n');
    $stmt->bindValue(':n',$nzv);
    $stmt->execute();
    $id= $stmt->fetch();
    $data['id_produkt']=$id['id_produkt'];
    //return $response->withHeader('Location',$this->router->pathFor('cena') . '?nazev_produkt=' . $nzv);
    return $this->view->render($response, 'cena.latte', $data);
    })->setName('smazat_cena');

//Oblíbené
    $app->get('/oblibene', function (Request $request, Response $response, $args) {
        $stmt = $this->db->prepare('SELECT id_oblibene, nazev, vyrobce from oblibene natural join produkt WHERE :idu=id_uzivatel');
        $stmt->bindValue(':idu',$_SESSION['prihlaseny_uzivatel']['id_uzivatel']);
        $stmt->execute();
        $data['produkt']=$stmt->fetchall();
       
        return $this->view->render($response, 'oblibene.latte', $data);
    })->setName('oblibene');
//Přidání do oblíbených
    $app->get('/{id_produkt}/oblibene', function (Request $request, Response $response, $args) {
        //$params = $request->getQueryParams();
        //$data['nazev_produkt'] = $args['nazev_produkt'];
        $idp = intval($args['id_produkt']);
        $stmt=$this->db->prepare('SELECT * FROM oblibene WHERE id_uzivatel=:idu');
        $stmt->bindValue(':idu',$_SESSION['prihlaseny_uzivatel']['id_uzivatel']);
        $stmt->execute();
        $obl = $stmt->fetchall();
        $jeStejne = 0;
        foreach($obl as $o){
            if($o['id_produkt']==$idp){
                $jeStejne = 1;
            }
        }
        if($jeStejne == 0){ //neni jeste v oblibenych
            try{
                $stmt = $this->db->prepare('INSERT INTO oblibene(id_uzivatel, id_produkt) VALUES(:idu,:idp)');
                $stmt->bindValue(':idu',$_SESSION['prihlaseny_uzivatel']['id_uzivatel']);
                $stmt->bindValue(':idp',$idp);
                $stmt->execute();
            }
            catch(Exception $e){
                echo var_dump($e);
            }
        } else {
            $data['message']="Tuto položku již máte v oblíbených";
        }

        $stmt = $this->db->prepare('SELECT id_oblibene, nazev, vyrobce from oblibene natural join produkt WHERE :idu=id_uzivatel');
        $stmt->bindValue(':idu',$_SESSION['prihlaseny_uzivatel']['id_uzivatel']);
        $stmt->execute();
        $data['produkt']=$stmt->fetchall();
        //$message = $params['id_produkt'];
       
        return $this->view->render($response, 'oblibene.latte', $data);
    })->setName('oblibene_pridat');
//Odebrat oblibene
    $app->get('/oblibene/{id_oblibene}/odebrat', function (Request $request, Response $response, $args) {
        $params = $request->getQueryParams();
        $stmt = $this->db->prepare('DELETE FROM oblibene WHERE id_oblibene=:ido');
        $stmt->bindValue(':ido',$args['id_oblibene']);
        $stmt->execute();
        $data['produkt']=$stmt->fetchall();
       
        return $response->withHeader('Location',$this->router->pathFor('oblibene'));
    })->setName('oblibene_odebrat');

//***KONEC GROUP***
})->add(function($request, $response, $next){
    if(!empty($_SESSION['prihlaseny_uzivatel'])){
        return $next($request, $response);
    }
    else{
        return $response->withHeader('Location',$this->router->pathFor('prihlasit'));
    }
});

//Kategorie i s výpisem kategorií (obviously)
$app->get('/kategorie', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();

    if(!empty($params['query'])){
        $stmt = $this->db->prepare('SELECT * FROM kategorie WHERE nazev = :n AND popis = :p');
        $stmt->bindParam(':n',$params['query']);
        $stmt->bindParam(':p',$params['query']);
        $stmt->execute();
        $data['kategorie']=$stmt->fetchall();
    }
    else{
        $stmt = $this->db->prepare('SELECT * FROM kategorie ORDER BY nazev');
        $stmt -> execute();

        $data['kategorie']=$stmt->fetchall();
    }
    return $this->view->render($response, 'kategorie.latte', $data);
})->setName('kategorie');

//Produkty z kategorie
$app->get('/{nazev_kategorie}/produkty', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();

    if(!empty($params['id_kategorie'])){
        $stmt = $this->db->prepare('SELECT * from kategorie LEFT JOIN produkt ON (kategorie.id_kategorie = produkt.id_kategorie) WHERE (:idk = kategorie.id_kategorie)');

        $stmt->bindValue(':idk',$params['id_kategorie']);

        $stmt->execute();

        $data['produkt'] = $stmt->fetchall();

        //$data['message'] = 'Jsem v ifu';
    }
    else{
        $stmt = $this->db->prepare('SELECT * FROM produkt ORDER BY vyrobce');

        $stmt -> execute();

        $data['produkt']=$stmt->fetchall();

        //$data['message'] = 'Nejsem v ifu';
    }
    //return $response->withHeader('Location', $this->router->pathFor('produkty'));
    //return $this->view->render($response, 'produkty.latte', $data);

    $data['nazev_kategorie'] = $args['nazev_kategorie'];
    //$data['nazev_produkt'] = $args['nazev_produkt'];

    return $this->view->render($response, 'produkty.latte', $data);
})->setName('produkty');

//Ceny pro produkt
$app->get('/{nazev_produkt}/cena', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    $data['nazev_produkt'] = $args['nazev_produkt'];
    $data['id_produkt'] = $params['id_produkt'];
    //$nazev_produkt = $args['nazev_produkt'];
    if(!empty($params['id_produkt'])){
        $stmt = $this->db->prepare('SELECT id_cena, nazev, vyrobce, cena, datum from produkt NATURAL JOIN cena_produkt WHERE (:idp = produkt.id_produkt) ORDER BY datum asc');
        $stmt->bindValue(':idp',$params['id_produkt']);
        $stmt->execute();
        $data['produkt'] = $stmt->fetchall();
    }
    else{
        $stmt = $this->db->prepare('SELECT id_cena, nazev, vyrobce, cena, datum from produkt NATURAL JOIN cena_produkt WHERE (:idp = produkt.id_produkt) ORDER BY datum asc');
        $stmt->bindValue(':idp',$params['id_produkt']);
        $stmt->execute();
        $data['produkt'] = $stmt->fetchall();
    }
    $stmt = $this->db->prepare('SELECT id_cena, nazev, vyrobce, cena, datum from produkt NATURAL JOIN cena_produkt WHERE (:idp = produkt.id_produkt) ORDER BY datum asc');
        $stmt->bindValue(':idp',$params['id_produkt']);
        $stmt->execute();
        $data['produkt'] = $stmt->fetchall();
    return $this->view->render($response, 'cena.latte', $data);
})->setName('cena');

//Přidání ceny k produktu
$app->post('/{nazev_produkt}/cena', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    $formData=$request->getParsedBody();
    $data['nazev_produkt'] = $args['nazev_produkt'];
    if(!empty($formData)){
        try{
            $stmt = $this->db->prepare('INSERT INTO cena_produkt(id_produkt, datum, cena, komentar) VALUES(:idp, :d, :c, :k)');

            $stmt->bindParam(':idp',$params['id_produkt']);
            $stmt->bindParam(':d',$formData['datum']);
            $stmt->bindParam(':c',$formData['cena']);
            $stmt->bindValue(':k',empty($formData['komentar']) ? null : $formData['komentar']);  

            $stmt->execute();

            $data['produkt'] = $stmt->fetchall();
    }
        catch(Exception $e){
            $data['message'] = 'Ooopsie...';
        }
    }
    $stmt = $this->db->prepare('SELECT id_cena, nazev, vyrobce, cena, datum from produkt NATURAL JOIN cena_produkt WHERE (:idp = produkt.id_produkt) ORDER BY datum asc');
    $stmt->bindValue(':idp',$params['id_produkt']);
    $stmt->execute();
    $data['produkt']=$stmt->fetchall();
    $data['id_produkt']=$params['id_produkt'];

    return $this->view->render($response, 'cena.latte', $data);
});