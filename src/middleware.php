<?php
// Application middleware

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->add(function(Request $request, Response $response, $next) {
    $basePath = $request->getUri()->getBasePath();
    $this->view->addParam('basePath', $basePath);
    if(isset($_SESSION['prihlaseny_uzivatel'])){
        $this->view->addParam('prihlaseny_uzivatel',$_SESSION['prihlaseny_uzivatel']);
    }
    return $next($request, $response);
});